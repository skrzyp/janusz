package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

// Offer is a main data unit in Janusz
type Offer struct {
	title     string
	link      string
	shortLink string
	price     string
	location  string
	source    string
}

var limit *int

func shortLink(link string) string {
	shortLinkURL := &url.URL{
		Scheme: "https",
		Host:   "is.gd",
		Path:   "create.php",
	}
	shortLinkQuery := shortLinkURL.Query()
	shortLinkQuery.Set("url", link)
	shortLinkQuery.Set("format", "simple")
	shortLinkURL.RawQuery = shortLinkQuery.Encode()
	shortLinkResp, _ := http.Get(shortLinkURL.String())
	shortLink, _ := ioutil.ReadAll(shortLinkResp.Body)
	return string(shortLink)
}

func queryOLX(query string) []Offer {
	offers := []Offer{}
	resp, err := http.Get(fmt.Sprintf("http://olx.pl/oferty/q-%s/", query))
	if err != nil {
		fmt.Printf("Coś mi się pojebało i nie mogłem dodzwonić się do OLX: %s\n", err)
	} else {
		doc, _ := goquery.NewDocumentFromReader(resp.Body)
		doc.Find(".hasPromoted > p:nth-child(2)").Each(func(i int, s *goquery.Selection) {
			fmt.Printf("%s\n\n", s.Text())
		})
		doc.Find(".offer-wrapper").Each(func(i int, s *goquery.Selection) {
			currentOffer := Offer{}
			if i <= *limit-1 {
				link, _ := s.Find("a.link").Attr("href")

				currentOffer.price = strings.TrimSpace(s.Find(".price").Text())
				if currentOffer.price == "" {
					currentOffer.price = "none"
				}
				currentOffer.title = strings.TrimSpace(s.Find("a.link").Text())
				currentOffer.link, _ = s.Find("a.link").Attr("href")
				currentOffer.shortLink = shortLink(link)
				currentOffer.source = "OLX"
				offers = append(offers, currentOffer)
			}
		})
	}
	return offers
}

func queryAllegro(query string) []Offer {
	offers := []Offer{}
	return offers
}

func main() {
	limit = flag.Int("limit", math.MaxInt64, "max number of offers listed")
	query := flag.String("query", "", "query to ask")
	flag.Parse()
	if *query == "" {
		flag.Usage()
		os.Exit(1)
	}
	fmt.Printf("Zapytanie: %s\n", *query)
	for i, offer := range queryOLX(*query) {
		fmt.Printf("%d: [%10s] %s - %s\n", i, offer.price, offer.shortLink, offer.title)
	}

}
